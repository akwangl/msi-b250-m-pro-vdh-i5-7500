# MSI-B250M PRO-VDH-i5-7500

#### Specs
| **Component** | **Model** |
| ------------- | --------- |
| CPU | i5-7500 |
| RAM | 金士顿DDR4 16GB (2x8GB) 2400MHz |
| Audio Chipset | Realtek ALC887. Works with layout-id 1 |
| dGPU | 盈通AMD RX560d |
| iGPU | Intel HD Graphics 630 |
| Lan |  Realtek 8111H Gigabit LAN controller |
| OS Disk | 512GB 铠侠 KBG40ZNV512G |
| macOS | macOS Ventura 13.2.1/OpenCore 0.9.0

#### BIOS
| **Setting** | **Value** |
| ------------- | --------- |
| Above 4G memory | Enabled |
| Initial Graphics Adapter | PEG if dGPU or IGD if iGPU |
| Intergrated Graphics Share Memory | 128MB |
| SATA Mode | AHCI Mode |
| Intel Serial I/O | Disabled |
| XHCI Hand-off | Enabled |
| Legacy USB Support | Auto |
| Windows 8.1/10 WHQL Support | Enabled |
| Windows 7 Installation | Disabled |
| Intel VT-D Tech | Enabled |
| CFG Lock | Disabled |

## USB 定制
### hackintool
![hackintool](./image/hackintool-usb.png)
### USBToolBox
![USBToolBox](./image/usb.png)
> **大部分进不去安装界面基本上都是USB定制问题**
